all: 
	@echo no default target..

%.pdf: %.svg
	inkscape --export-pdf="$@" "$<"

%.png: %.svg
	inkscape --export-png="$@" "$<"

%.png: %.dia
	dia --export="$@" "$<"

%.pdf: %.dia
	dia --export="$@" "$<"

%.png: %.dot
	dot -Tpng -o $@ $<

%.pdf: %.tex
	test -d _output || mkdir _output
	pdflatex -output-directory _output $<
	pdfcrop _output/$@ _output/cropped.pdf
	cp _output/cropped.pdf $@

# dependencies
power-market.pdf: power-market.tikz

